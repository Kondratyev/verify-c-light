(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/typed-lists/top" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(define grt-eql-cnt
    (
        (n integerp)
        (key integerp)
        (a integer-listp)
    )
    :guard-hints
    (
        ("Goal"
            :induct (dec-induct n)
        )
    )
    :returns
    (result natp
        :hints
        (
            ("Goal"
                :induct (dec-induct n)
            )
        )
    )
    (b*
        (
            (n (nfix n))
            (key (ifix key))
            (a (integer-list-fix a))
            ((when (zp n)) 0)
            ((when (< (len a) n)) 0)
        )
        (if
            (<=             
                key
                (nth (- n 1) a)
            )
            (+ 1 (grt-eql-cnt (- n 1) key a))
            (grt-eql-cnt (- n 1) key a)
        )
    )
    ///
    (fty::deffixequiv grt-eql-cnt)
)

