(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/typed-lists/top" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(fty::defprod frame
    ((loop-break booleanp)
     (i integerp)
     (result integerp)))

(fty::defprod envir
    ((lower-bound integerp)
     (key integerp)
     (a integer-listp)))

(define frame-init
    ((i integerp)
     (result integerp))
    :returns (fr frame-p)
    (make-frame
        :loop-break nil
        :i i
        :result result
    )
    ///
    (fty::deffixequiv frame-init))

(define envir-init
    ((lower-bound integerp)
     (key integerp)
     (a integer-listp))
    :returns (env envir-p)
    (make-envir
        :lower-bound lower-bound
        :key key
        :a a
    )
    ///
    (fty::deffixequiv envir-init))

(define rep
    (
        (iteration natp)
        (env envir-p)
        (fr frame-p)
    )
    :measure (nfix iteration)
    :verify-guards nil
    :returns (upd-fr frame-p)
    (b*
        (
            (iteration (nfix iteration))
            (env (envir-fix env))
            (fr (frame-fix fr))        
            ((when (zp iteration)) fr)
            (fr (rep (- iteration 1) env fr))
            ((when (frame->loop-break fr)) fr)        
            (fr
                (if
                    (<
                        (nth
                            (-
                                (+
                                    iteration
                                    (envir->lower-bound env)
                                )
                                1
                            )
                            (envir->a env)
                        )
                        (envir->key env)
                    )
                    (b*
                        (
                            (fr
                                (change-frame fr
                                    :result
                                    1
                                )
                            )
                            (fr
                                (change-frame fr
                                    :loop-break
                                    t
                                )
                            )
                            ((when t) fr)
                        )
                        fr
                    )
                    (b*
                        (
                            (fr fr)
                        )
                        fr
                    )
                )
            )
            ((when (frame->loop-break fr)) fr)
            (fr
                 (change-frame fr
                     :i
                     (+
                         iteration
                         (envir->lower-bound env)
                     )
                 )
            )
        )
        fr
    )
)

