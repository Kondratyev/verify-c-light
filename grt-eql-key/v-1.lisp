(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/typed-lists/top" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(include-book "grt-eql-cnt")
(include-book "rep")

(defrule v-1-lemma-1
    (implies
        (and
            (< 0 n)
            (<= n (len a))
            (integerp n)
            (integerp key)
            (integer-listp a)
            (= 0 (grt-eql-cnt n key a))
        )
        (frame->loop-break
            (rep
                n
                (envir-init
                    0
                    key
                    a
                )
                (frame-init
                    0
                    0
                )
            )
        )
    )
    :enable
    (
        grt-eql-cnt
        envir-init
        frame-init
        rep
    )
    :hints
    (
        ("Goal"
            :induct (dec-induct n)
        )
    )
)

(defrule v-1
    (implies
        (and
            (< 0 n)
            (<= n (len a))
            (integerp n)
            (integerp key)
            (integer-listp a)
            (= 0 (grt-eql-cnt n key a))
        )
        (not
            (=
                (frame->result
                    (rep
                        n
                        (envir-init
                            0
                            key
                            a
                        )
                        (frame-init
                            0
                            0
                        )
                    )
                )
                0
            )
        )
    )
    :enable
    (
        grt-eql-cnt
        envir-init
        frame-init
        rep
        v-1-lemma-1
    )
    :hints
    (
        ("Goal"
            :induct (dec-induct n)
        )
    )
)

