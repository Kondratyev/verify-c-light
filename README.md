The folder grt-eql-key hosts all data for verification experiment for program of the same name. The program under discussion checks whether array a (with length n) contains an element greater or equal to key. We deliberately made an error in program. The folder includes the underlying theory, the replacement function theory as well as theory of the lemma v-1 that was generated for unsatisfiability proof.

Each file in repository is a theory - a set of definitions and theorems. Thus, every such file is a library and forms the module of the ACL2 proof system. Such a module in the ACL2 system is called a book.

To prove all the theorems of the book, a process called book certification is launched. The book is called certified when all its theorems are proved. Therefore, to prove the desired theorem, add it to the book and certify the book.

In order to use a theory from another book in a given one, it is necessary to prescribe the inclusion of another book in the given book using the "include-book" construct. In order to certify a book, included books must also be certified.
